attiny85_fast_pwm
=================

Demonstrate multiple output Fast PWM on ATTINY85.

Target - attiny85 on STK500 or standalone

Execute the following

    make clean
    make
    make dl

Now, PORTB0 and PORTB1 will toggle according to the frequency and duty cycle set.

With 1MHz clock (/8 set by fuse), and prescaler = 64, Frequency should be

``` text
f_pwm = f_clk/(N * 256) = 1000000 / (64 * 256) = 61.035 Hz
```

As we are using internal oscillator, the measured frequency will be near this
calculated frequency.

Note the division by 256. This reflects the size (8 bits) of Timer0


Build Output
------------


``` text
11:18 $ make dl PORT=/dev/cu.usbserial-AK05DQMH
/usr/local/bin/avrdude -v -c stk500v2 -p attiny85 -P /dev/cu.usbserial-AK05DQMH   -U flash:w:main.hex -U lfuse:w:0x62:m

avrdude: Version 6.3, compiled on Sep 17 2016 at 02:19:28
         Copyright (c) 2000-2005 Brian Dean, http://www.bdmicro.com/
         Copyright (c) 2007-2014 Joerg Wunsch

         System wide configuration file is "/usr/local/Cellar/avrdude/6.3/etc/avrdude.conf"
         User configuration file is "/Users/frank/.avrduderc"
         User configuration file does not exist or is not a regular file, skipping

         Using Port                    : /dev/cu.usbserial-AK05DQMH
         Using Programmer              : stk500v2
         AVR Part                      : ATtiny85
         Chip Erase delay              : 4500 us
         PAGEL                         : P00
         BS2                           : P00
         RESET disposition             : possible i/o
         RETRY pulse                   : SCK
         serial program mode           : yes
         parallel program mode         : yes
         Timeout                       : 200
         StabDelay                     : 100
         CmdexeDelay                   : 25
         SyncLoops                     : 32
         ByteDelay                     : 0
         PollIndex                     : 3
         PollValue                     : 0x53
         Memory Detail                 :

                                  Block Poll               Page                       Polled
           Memory Type Mode Delay Size  Indx Paged  Size   Size #Pages MinW  MaxW   ReadBack
           ----------- ---- ----- ----- ---- ------ ------ ---- ------ ----- ----- ---------
           eeprom        65     6     4    0 no        512    4      0  4000  4500 0xff 0xff
           flash         65     6    32    0 yes      8192   64    128  4500  4500 0xff 0xff
           signature      0     0     0    0 no          3    0      0     0     0 0x00 0x00
           lock           0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           lfuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           hfuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           efuse          0     0     0    0 no          1    0      0  9000  9000 0x00 0x00
           calibration    0     0     0    0 no          1    0      0     0     0 0x00 0x00

         Programmer Type : STK500V2
         Description     : Atmel STK500 Version 2.x firmware
         Programmer Model: STK500
         Hardware Version: 2
         Firmware Version Master : 2.01
         Topcard         : Unknown
         Vtarget         : 5.1 V
         SCK period      : 35.3 us
         Varef           : 5.0 V
         Oscillator      : 3.686 MHz

avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.01s

avrdude: Device signature = 0x1e930b (probably t85)
avrdude: safemode: hfuse reads as DF
avrdude: safemode: efuse reads as FF
avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
         To disable this feature, specify the -D option.
avrdude: erasing chip
avrdude: reading input file "main.hex"
avrdude: input file main.hex auto detected as Intel Hex
avrdude: writing flash (86 bytes):

Writing | ################################################## | 100% 0.13s

avrdude: 86 bytes of flash written
avrdude: verifying flash memory against main.hex:
avrdude: load data flash data from input file main.hex:
avrdude: input file main.hex auto detected as Intel Hex
avrdude: input file main.hex contains 86 bytes
avrdude: reading on-chip flash data:

Reading | ################################################## | 100% 0.18s

avrdude: verifying ...
avrdude: 86 bytes of flash verified
avrdude: reading input file "0x62"
avrdude: writing lfuse (1 bytes):

Writing | ################################################## | 100% 0.02s

avrdude: 1 bytes of lfuse written
avrdude: verifying lfuse memory against 0x62:
avrdude: load data lfuse data from input file 0x62:
avrdude: input file 0x62 contains 1 bytes
avrdude: reading on-chip lfuse data:

Reading | ################################################## | 100% 0.00s

avrdude: verifying ...
avrdude: 1 bytes of lfuse verified

avrdude: safemode: hfuse reads as DF
avrdude: safemode: efuse reads as FF
avrdude: safemode: Fuses OK (E:FF, H:DF, L:62)

avrdude done.  Thank you.


```

Example outputs on PB0 and PB1.


![Screenshot](images/DS2_20171213112152.png)


AVR Pinout
----------

Here is the pinout for the ATtiny25/45/85 series


![Screenshot](images/attiny85.png)


Clock Output
------------

By setting fuse, we can get clock output on PORTB4.

``` text
make dl_clock_out PORT=/dev/cu.usbserial-AK05DQMH
```

Here is Clock Out with no adjustment to OSCCAL.

![Screenshot](images/DS2_20171213162530.png)

Here is clock output with the following adjustment.



``` text
OSCCAL -=4;
```

![Screenshot](images/DS2_20171213162616.png)
