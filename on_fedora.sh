#!/usr/bin/env bash

# Set .clang_complete for Fedora 30 environment
ln -sf .clang_complete_fedora .clang_complete

# Set .dir-locals for Fedora environment
ln -sf .dir-locals-fedora.el .dir-locals.el
