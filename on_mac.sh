#!/usr/bin/env bash

# Set .clang_complete for Mac environment
ln -sf .clang_complete_mac .clang_complete

# Set .dir-locals for Mac environment
ln -sf .dir-locals-mac.el .dir-locals.el

