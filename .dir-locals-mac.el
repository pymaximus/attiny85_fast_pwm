;; Allow flycheck to use clang or gcc with C11


((c-mode
  (flycheck-clang-language-standard . "gnu11")
  (flycheck-gcc-language-standard . "gnu11")
  (flycheck-gcc-args . ("-Wall"
                        "-Os"
                        "-DF_CPU=1000000UL"
                        "-Wno-ignored-attributes"
                        "-Wno-attributes"
                        "-D__AVR_ATtiny85__"))
  
  (flycheck-clang-args . ("-Wall"))

  (flycheck-c/c++-gcc-executable . "/usr/local/bin/avr-gcc")
  ;; c/c++-gcc works well, so disbale cland and irony
  (flycheck-disabled-checkers . (c/c++-clang irony))
  (flycheck-gcc-include-path . (
                                "/usr/local/Cellar/avr-gcc/9.1.0/avr/include"
                                "/usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr"
                                "/usr/local/Cellar/avr-gcc/9.1.0/avr/include/sys"
                                "/usr/local/Cellar/avr-gcc/9.1.0/avr/include/util"
                                ))
  ))
