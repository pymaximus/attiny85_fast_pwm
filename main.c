/******************************************************************************
 Title:    Fast PWM Example, multiple outputs
 Author:   Frank Singleton
 Hardware: Attiny85 at 1Mhz, STK200 compatible starter kit

 Description:
 This example demonstrates fast PWM mode. In this case 2 outputs
*******************************************************************************/

#define F_CPU 1000000UL

#include <inttypes.h>
#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


/********************************************************************************
 Main
********************************************************************************/
int main(void) {

    /* disable global interrupts */
    cli();

    // set calibration delta, by observing PB4 output on scope.
    // change this until you get close to 1MHz output.
    OSCCAL -=4;

    /* Set pin PB0/PB1 as output */
    DDRB |= (1<<PB0) | (1<<PB1);

    /* init timer0 */
    TCCR0A = 0;
    TCCR0B = 0;

    TCCR0A |= (1<<WGM01) | (1<<WGM00) | (1<<COM0A1) | (1<<COM0B1) | (1<<COM0B0);

    TCCR0B |= (1 << CS00) | (1 << CS01); /* CLK/64 ,Prescaler table 11.6 */

    // These can be set independently, but for now set them the same
    OCR0A = 39; /* CTC Compare value, 40 = 39 + 1 */
    OCR0B = 39; /* CTC Compare value, 40 = 39 + 1 */

    /* enable global interrupts only if required */
    //sei();

    while (true) {
        ;
    }
}
